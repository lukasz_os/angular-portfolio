import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { Post } from './post.model';

@Injectable()
export class PostService {
    private baseURL = 'http://blog.lukaszswiderski.pl/wp-json/wp/v2/';
    private posts: Post[] = [];
    postChanged = new Subject<Post[]>();
    postGetted = new Subject<Post>();
    constructor(
        private http: Http
    ) {}

    getPosts() {
        this.http.get( this.baseURL + 'posts')
            .pipe(
                map(response => {
                const posts: Post[] = response.json();
                return posts;
        })
        ).subscribe(
            (posts: Post[]) => {
              this.storePosts(posts);
              this.postChanged.next(this.posts.slice());
            }
          );
    }

    getPost(slug: string) {
        this.http.get(this.baseURL + 'posts?slug=' + slug)
            .pipe(
                map(response => {
                const post: Post = response.json();
                return post[0];
        })
        ).subscribe(
            (post: Post) => {
              this.posts.push(post);
              this.postGetted.next(post);
            }
          );
    }

    storePosts(posts) {
        this.posts = posts;
    }

    storePost(post) {
        this.posts.push(post);
        this.postChanged.next(this.posts.slice());
    }

    // getPostBySlug(slug: string) {
    //     let postToReturn: Post;
    //     this.posts.forEach(post => {

    //         if ( post.slug === slug ) {
    //             postToReturn = post ;
    //         }
    //     });
    //     if ( postToReturn === undefined) {
    //         this.getPostBySlug(slug);
    //     } else {
    //         return postToReturn;
    //     }
    // }

}
