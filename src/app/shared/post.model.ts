export class Post {
    public id: number;
    public date: string;
    public date_gmt: string;
    public guid: {rendered: string};
    public modified: string;
    public modified_gmt: string;
    public slug: string;
    public status: string;
    public type: string;
    public link: string;
    public title: {rendered: string};
    public content: {rendered: string};
    public excerpt: {rendered: string};
    public author: number;
    public featured_media: number;
    public comment_status: string;
    public ping_status: string;
    public sticky: boolean;
    public template: string;
    public format: string;
    public meta: string[];
    public categories: number[];
    public tags: number[];
    public _links: {};

    constructor() {}
 }
