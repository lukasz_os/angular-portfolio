import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Post } from '../shared/post.model';
import { PostService } from '../shared/post.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})

export class PostComponent implements OnInit, OnDestroy {
  private slug: string;
  post: Post;
  subscription: Subscription;
  isLoaded = false;
  constructor(private postService: PostService, router: Router, private route: ActivatedRoute ) {

    this.route.params
      .subscribe(
        (params: Params) => {
          this.slug = params['slug'];
          this.subscription = this.postService.postGetted.subscribe(
            (post: Post) => {
              this.post = post;
              this.isLoaded = true;
            }
          );
          this.postService.getPost(this.slug);
        }
      );
  }

  ngOnInit() {
  }
  ngOnDestroy() {

  }
}
